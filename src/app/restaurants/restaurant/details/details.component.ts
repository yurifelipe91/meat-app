import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantsService } from 'app/restaurants/restaurants.service';
import { Restaurant } from '../restaurant.model';

@Component({
	selector: 'mt-details',
	templateUrl: './details.component.html',
	styleUrls: [ './details.component.css' ]
})
export class DetailsComponent implements OnInit {
	restaurant: Restaurant;

	constructor(private restaurantsService: RestaurantsService, private route: ActivatedRoute) {}

	ngOnInit() {
		this.restaurantsService
			.restaurantById(this.route.snapshot.params['id']) //Snapshot é a captura dos status dos parametros no momento do acesso. Como só faremos isso 1x, nao precisa utilizar subscribe.
			.subscribe((r) => (this.restaurant = r));
	}
}
