import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MEAT_API } from "app/app.api";
import { CartItem } from "app/restaurants/restaurant/details/shopping-cart/cart-item.model";
import { ShoppingCartService } from "app/restaurants/restaurant/details/shopping-cart/shopping-cart.service";
import { Observable } from "rxjs";
import { Order } from "./order.model";

@Injectable()
export class OrderService {
  deliveryInService: number = 8;

  constructor(
    private _cartService: ShoppingCartService,
    private http: HttpClient
  ) {}

  itemsValue(): number {
    return this._cartService.total();
  }

  cartItems(): CartItem[] {
    return this._cartService.items;
  }

  increaseQty(item: CartItem) {
    this._cartService.increaseQty(item);
  }

  decreaseQty(item: CartItem) {
    this._cartService.decreaseQty(item);
  }

  remove(item: CartItem) {
    this._cartService.removeItem(item);
  }

  clear() {
    this._cartService.clear();
  }

  checkOrderService(order: Order): Observable<string> {
    return this.http
      .post<Order>(`${MEAT_API}/orders`, order)
      .map(order => order.id);
  }
}
