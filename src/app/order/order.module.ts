import { NgModule } from "@angular/core";
import { SharedModule } from "app/shared/shared.module";
import { DeliveryCostsComponent } from "./delivery-costs/delivery-costs.component";
import { OrderItemsComponent } from "./order-items/order-items.component";
import { OrderRoutingModule } from "./order-routing.module";
import { OrderComponent } from "./order.component";

@NgModule({
  imports: [SharedModule, OrderRoutingModule],
  declarations: [OrderComponent, OrderItemsComponent, DeliveryCostsComponent],
})
export class OrderModule {}
