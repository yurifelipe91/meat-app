import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import { CartItem } from "app/restaurants/restaurant/details/shopping-cart/cart-item.model";
import { RadioOption } from "app/shared/radio/radio-option.model";
import { Order, OrderItem } from "./order.model";
import { OrderService } from "./order.service";

import "rxjs/add/operator/do";

@Component({
  selector: "mt-order",
  templateUrl: "./order.component.html",
  styleUrls: ["./order.component.css"],
})
export class OrderComponent implements OnInit {
  deliveryInOrder: number;
  paymentOptions: RadioOption[] = [
    { label: "Dinheiro", value: "MON" },
    { label: "Cartão de Débito", value: "DEB" },
    { label: "Cartão Refeição", value: "REF" },
  ];
  orderId: string;
  orderForm: FormGroup;
  emailPattern =
    /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  numberPattern = /^[0-9]*$/;

  constructor(
    private _orderService: OrderService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.deliveryInOrder = this._orderService.deliveryInService;
  }

  ngOnInit() {
    this.orderForm = new FormGroup(
      {
        name: new FormControl("", {
          validators: [Validators.required, Validators.minLength(5)],
        }),
        email: new FormControl("", {
          validators: [
            Validators.required,
            Validators.pattern(this.emailPattern),
          ],
        }),
        emailConfirmation: new FormControl("", {
          validators: [
            Validators.required,
            Validators.pattern(this.emailPattern),
          ],
        }),
        address: new FormControl("", {
          validators: [Validators.required, Validators.minLength(5)],
        }),
        number: new FormControl("", {
          validators: [
            Validators.required,
            Validators.pattern(this.numberPattern),
          ],
        }),
        optionalAddress: new FormControl(""),
        paymentOption: new FormControl("", [Validators.required]),
      },
      { validators: [OrderComponent.equalsTo], updateOn: "blur" }
      /* updateOn: só dispara o validador após o campo deixar de ter o foco.*/
    );
  }

  static equalsTo(group: AbstractControl): { [key: string]: boolean } {
    const email = group.get("email");
    const emailConfirmation = group.get("emailConfirmation");
    if (!email || !emailConfirmation) {
      return undefined;
    }

    if (email.value !== emailConfirmation.value) {
      return { emailNotMatch: true };
    }
    return undefined;
  }

  itemsValueInOrder(): number {
    return this._orderService.itemsValue();
  }

  cartItems(): CartItem[] {
    return this._orderService.cartItems();
  }

  increaseQty(item: CartItem) {
    this._orderService.increaseQty(item);
  }

  decreaseQty(item: CartItem) {
    this._orderService.decreaseQty(item);
  }

  remove(item: CartItem) {
    this._orderService.remove(item);
  }

  onOrderCompleted(): boolean {
    return this.orderId !== undefined;
  }

  checkOrder(order: Order) {
    order.orderItems = this.cartItems().map(
      (item: CartItem) => new OrderItem(item.quantity, item.menuItem.id)
    );
    this._orderService
      .checkOrderService(order)
      .do((orderId: string) => {
        this.orderId = orderId;
      })
      .subscribe((orderId: string) => {
        this.router.navigate(["/order-summary"]);
        this._orderService.clear();

        console.log(orderId);
      });

    console.log(order);
  }
}
