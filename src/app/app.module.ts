import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { ErrorHandler, LOCALE_ID, NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { PreloadAllModules, RouterModule } from "@angular/router";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { HomeComponent } from "./home/home.component";
import { ROUTES } from "./app-routing.module";
import { RestaurantsComponent } from "./restaurants/restaurants.component";
import { RestaurantComponent } from "./restaurants/restaurant/restaurant.component";
import { DetailsComponent } from "./restaurants/restaurant/details/details.component";
import { MenuComponent } from "./restaurants/restaurant/details/menu/menu.component";
import { ShoppingCartComponent } from "./restaurants/restaurant/details/shopping-cart/shopping-cart.component";
import { MenuItemComponent } from "./restaurants/restaurant/details/menu-item/menu-item.component";
import { ReviewsComponent } from "./restaurants/restaurant/details/reviews/reviews.component";
import { OrderSummaryComponent } from "./order-summary/order-summary.component";
import { SharedModule } from "./shared/shared.module";
import { NotFoundComponent } from "./not-found/not-found.component";
import {
  HashLocationStrategy,
  LocationStrategy,
  registerLocaleData,
} from "@angular/common";

import locatePt from "@angular/common/locales/pt";
registerLocaleData(locatePt, "pt");

import { LoginComponent } from "./security/login/login.component";
import { UserDetailComponent } from "./header/user-detail/user-detail.component";
import { ApplicationErrorHandler } from "./app.error-handler";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    RestaurantsComponent,
    RestaurantComponent,
    DetailsComponent,
    MenuComponent,
    ShoppingCartComponent,
    MenuItemComponent,
    ReviewsComponent,
    OrderSummaryComponent,
    NotFoundComponent,
    LoginComponent,
    UserDetailComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,

    // PreloadAllModules permite pré carregar todos os modulos configurados quando a aplicacao é startada. Assim permite o acesso mais rapido aos componentes declarados nos modulos e setados na rota: loadChildren: "./order/order.module#OrderModule" - declarations: [OrderComponent, OrderItemsComponent, DeliveryCostsComponent] - modulos carregados no start da aplicaçao;
    RouterModule.forRoot(ROUTES, {
      preloadingStrategy: PreloadAllModules,
    }),
    SharedModule.forRoot(),
  ],
  providers: [
    // add o provide LocationStrategy e HashLocationStrategy para a aplicação se tornar visivel após o build
    { provide: LOCALE_ID, useValue: 'pt' },
    { provide: ErrorHandler, useClass: ApplicationErrorHandler },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
