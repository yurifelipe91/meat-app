import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { Component, Input, OnInit } from "@angular/core";
import { Restaurant } from "./restaurant.model";

@Component({
  selector: "mt-restaurant",
  templateUrl: "./restaurant.component.html",
  styleUrls: ["./restaurant.component.css"],
  animations: [
    trigger("restaurantAppeared", [
      state(
        "ready",
        style({
          opacity: 1,
        })
      ),
      transition("void => ready", [
        style({ opacity: 0, transform: "translateY(-20px)" }),
        animate("400ms 0s ease-in"),
      ]),
    ]),
  ],
})
export class RestaurantComponent implements OnInit {
  @Input() restaurant: Restaurant;

  restaurantState = "ready";

  constructor() {}

  ngOnInit() {}
}
