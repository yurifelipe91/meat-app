import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MEAT_API } from "app/app.api";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/Observable";
import { MenuItem } from "./restaurant/details/menu-item/menu-item.model";
import { Restaurant } from "./restaurant/restaurant.model";

@Injectable()
export class RestaurantsService {
  constructor(private http: HttpClient) {}

  restaurants(search?: string): Observable<Restaurant[]> {
    let params: HttpParams = undefined;
    if (search) {
      params = new HttpParams().append("q", search);
    }
    //aqui estou querendo todos os restaurantes, por isso está asssim: Restaurant[]
    return this.http.get<Restaurant[]>(`${MEAT_API}/restaurants`, {
      params,
    });
  }

  restaurantById(id: string): Observable<Restaurant> {
    //aqui estou querendo apenas um restaurante, por isso esta sem []
    return this.http.get<Restaurant>(`${MEAT_API}/restaurants/${id}`);
  }

  reviewsOfRestaurant(id: string): Observable<any> {
    return this.http.get(`${MEAT_API}/restaurants/${id}/reviews`);
  }

  menuOfRestaurant(id: string): Observable<MenuItem[]> {
    return this.http.get<MenuItem[]>(`${MEAT_API}/restaurants/${id}/menu`);
  }
}
