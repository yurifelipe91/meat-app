import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantsService } from 'app/restaurants/restaurants.service';
import { Observable } from 'rxjs/Observable';

import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
@Component({
  selector: "mt-reviews",
  templateUrl: "./reviews.component.html",
  styleUrls: ["./reviews.component.css"],
  animations: [
    trigger("reviewsAppeared", [
      state(
        "ready",
        style({
          opacity: 1,
        })
      ),
      transition("void => ready", [
        style({ opacity: 0, transform: "translateY(-20px)" }),
        animate("400ms 0s ease-in-out"),
      ]),
    ]),
  ],
})
export class ReviewsComponent implements OnInit {
  reviews: Observable<any>;

  reviewsState = "ready";

  constructor(
    private restaurantsService: RestaurantsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.reviews = this.restaurantsService.reviewsOfRestaurant(
      this.route.parent.snapshot.params["id"]
    );
  }
}
