import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MEAT_API } from "app/app.api";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/filter";
import { User } from "./user.model";
import { NavigationEnd, Router } from "@angular/router";

@Injectable()
export class LoginService {
  user: User;
  lastUrl: string;

  constructor(private http: HttpClient, private router: Router) {
    /*recuperando a ultima URL acessada*/
    this.router.events
      .filter((e) => e instanceof NavigationEnd)
      .subscribe((e: NavigationEnd) => {
        this.lastUrl = e.url;
        console.log("ultima url (subscribe): " + e);
      });
  }

  isLoogedIn(): boolean {
    return this.user !== undefined;
  }

  postLogin(email: string, password: string): Observable<User> {
    return this.http
      .post<User>(`${MEAT_API}/login`, {
        email: email,
        password: password,
      })
      .do((user) => {
        this.user = user;
      });
  }

  isLogout() {
    this.user = undefined;
    this.router.navigate(["/login"]);
  }

  /* se nenhum parametro for passado na URL, este método inclui na hora de logar a ultima URL acessada */
  handleLogin(path: string = this.lastUrl) {
    this.router.navigate(["/login", btoa(path)]);
  }
}
