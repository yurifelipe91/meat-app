export class User {
  constructor(
    public email: string,
    public name: string,
    private password: string
  ) {}

  matches(anothes: User): boolean {
    return (
      anothes !== undefined &&
      anothes.email === this.email &&
      anothes.password === this.password
    );
  }
}

export const users = {
  "yurii.felipe@gmail.com": new User(
    "yurii.felipe@gmail.com",
    "Yuri Andrade",
    "123"
  ),
  "karen.cdutra@gmail.com": new User(
    "karen.cdutra@gmail.com",
    "Karen Andrade",
    "saponalagoa"
  ),
};
