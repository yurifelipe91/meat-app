import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificationService } from "app/shared/messages/notification.service";
import { LoginService } from "./login.service";

@Component({
  selector: "mt-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  email = "Formato inválido!";
  pwd = "O campo senha precisa ser preenchido.";
  loginForm: FormGroup;
  navigateTo: string;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _loginService: LoginService,
    private _notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.loginForm = new FormGroup(
      {
        email: new FormControl("", {
          validators: [Validators.required, Validators.email],
        }),
        password: new FormControl("", {
          validators: [Validators.required],
        }),
      },
      { updateOn: "blur" }
    );

    this.navigateTo = this.activatedRoute.snapshot.params["to"] || btoa("/");
  }

  onLogin() {
    this._loginService
      .postLogin(this.loginForm.value.email, this.loginForm.value.password)
      .subscribe(
        (user) => this._notificationService.notify(`Bem vindo, ${user.name}`),
        (response) /*HttpErrorResponse*/ =>
          this._notificationService.notify(response.error.message),
        () => {
          this.router.navigate([atob(this.navigateTo)]);
        }
      );
  }
}

/* os métodos btoa e atob que encapsulam a rotas servem para organizar as URLs, evitando ter formatos estranhos %as43% */
