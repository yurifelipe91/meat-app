import { TestBed, async, inject } from '@angular/core/testing';

import { LeaveOrderGuard } from './leave-order.guard';

describe('LeaveOrderGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LeaveOrderGuard]
    });
  });

  it('should ...', inject([LeaveOrderGuard], (guard: LeaveOrderGuard) => {
    expect(guard).toBeTruthy();
  }));
});
