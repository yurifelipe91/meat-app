import { LoginComponent } from "./security/login/login.component";
import { Routes } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { OrderSummaryComponent } from "./order-summary/order-summary.component";

import { DetailsComponent } from "./restaurants/restaurant/details/details.component";
import { MenuComponent } from "./restaurants/restaurant/details/menu/menu.component";
import { ReviewsComponent } from "./restaurants/restaurant/details/reviews/reviews.component";
import { RestaurantsComponent } from "./restaurants/restaurants.component";
import { LoggedInGuard } from "./security/loggedin.guard";

export const ROUTES: Routes = [
  // {
  //   path: "",
  //   redirectTo: "login",
  //   pathMatch: "full",
  // },
  {
    path: "login/:to",
    component: LoginComponent,
  },
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "",
    component: HomeComponent,
  },
  {
    path: "about",
    loadChildren: "./about/about.module#AboutModule",
  },
  {
    path: "restaurants",
    component: RestaurantsComponent,
  },
  {
    path: "restaurants/:id",
    component: DetailsComponent,
    children: [
      {
        path: "",
        redirectTo: "menu",
        pathMatch: "full",
      },
      {
        path: "menu",
        component: MenuComponent,
      },
      {
        path: "reviews",
        component: ReviewsComponent,
      },
    ],
  },
  {
    canLoad: [LoggedInGuard],
    canActivate: [LoggedInGuard],
    path: "order",
    loadChildren: "./order/order.module#OrderModule",
  },
  {
    path: "order-summary",
    component: OrderSummaryComponent,
  },
  {
    path: "**",
    component: NotFoundComponent,
  },
];
