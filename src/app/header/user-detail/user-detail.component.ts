import { Component, OnInit } from "@angular/core";
import { LoginService } from "app/security/login/login.service";
import { User } from "app/security/login/user.model";

@Component({
  selector: "mt-user-detail",
  templateUrl: "./user-detail.component.html",
  styleUrls: ["./user-detail.component.css"],
})
export class UserDetailComponent implements OnInit {
  constructor(private _loginService: LoginService) {}

  ngOnInit() {}

  onUser(): User {
    return this._loginService.user;
  }

  onLoggedIn(): boolean {
    return this._loginService.isLoogedIn();
  }

  onLogin() {
    this._loginService.handleLogin();
  }

  onLogout() {
    this._loginService.isLogout();
  }
}
